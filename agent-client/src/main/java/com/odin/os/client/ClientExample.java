package com.odin.os.client;

import com.odin.os.ExecutorGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.odin.os.ExecutorTypes.*;

/**
 * @author Daniil Mikhaylov <dmikhaylov@odin.com>
 * @since 24.11.17 16:31
 */
public class ClientExample
{
    private static final Logger logger = Logger.getLogger(ClientExample.class.getName());

    private final ManagedChannel channel;
    private final ExecutorGrpc.ExecutorBlockingStub blockingStub;

    public ClientExample(String host, int port) {
        this(
            ManagedChannelBuilder.forAddress(host, port)
            // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
            // needing certificates.
            .usePlaintext(true)
            .build()
        );
    }

    ClientExample(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = ExecutorGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    protected Operation createOp(String customCommand, String value, boolean isSecured) {
        return Operation.newBuilder()
            .setOperationType(Operation.OperationType.EXECUTE)
            .addProperties(
                Operation.Property.newBuilder()
                    .setKey(customCommand)
                    .setValue(value)
                    .setIsSecured(isSecured)
                    .build()
            )
            .build();
    }

    public void testExecute(Operation op) {
        logger.info("Start executing test command:" + op.getOperationType());

        executeWrapper(() -> blockingStub.execute(op));
    }

    public void testExecuteBulk(List<Operation> listOps) {
        executeWrapper(() -> blockingStub.executeBulk(BulkOperation.newBuilder().addAllOperations(listOps).build()));
    }

    protected void executeWrapper(Supplier<OperationResult> executor) {
        OperationResult result;

        try {
            result = executor.get();
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return;
        }

        logger.info("Operation result: " + result.getResult());
    }

    public static void main(String[] args) throws Exception {
        ClientExample client = new ClientExample("localhost", 6565);

        try {
            client.testExecute(client.createOp("rm -rf", "~", false));
            // some shit just for example
            client.testExecuteBulk(Arrays.asList(
                client.createOp("set-password-blabla", "1234556", true),
                client.createOp("chown", "1234556", false),
                client.createOp("chmod", "1234556", false)
            ));
        } finally {
            client.shutdown();
        }
    }
}
