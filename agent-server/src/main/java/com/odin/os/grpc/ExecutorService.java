package com.odin.os.grpc;

import com.odin.os.ExecutorGrpc;
import com.odin.os.ExecutorTypes;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

import static com.odin.os.ExecutorTypes.*;

/**
 * @author Daniil Mikhaylov <dmikhaylov@odin.com>
 * @since 23.11.17 15:04
 */
@GRpcService
public class ExecutorService extends ExecutorGrpc.ExecutorImplBase
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void execute(Operation request, StreamObserver<ExecutorTypes.OperationResult> responseObserver) {
        logger.info("Execute ({}) with params: {}", request.getOperationType(), request.getPropertiesList());

        OperationResult.Builder builder  = OperationResult.newBuilder()
            .setStatus(OperationResult.Status.OK)
            .setResult(String.format("Execute (%s)", request.getOperationType()));

        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void executeBulk(BulkOperation request, StreamObserver<ExecutorTypes.OperationResult> responseObserver) {
        logger.info("Execute bulk operations : {}", request.getOperationsList());

        request
            .getOperationsList()
            .forEach(o -> logger.info("Execute ({}) with params: {}", o.getOperationType(), o.getPropertiesList()));

        responseObserver.onNext(OperationResult.newBuilder()
            .setStatus(OperationResult.Status.OK)
            .setResult("All operation executed").build());
        responseObserver.onCompleted();
    }
}
