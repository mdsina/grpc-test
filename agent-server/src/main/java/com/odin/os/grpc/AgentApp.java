package com.odin.os.grpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Daniil Mikhaylov <dmikhaylov@odin.com>
 * @since 23.11.17 15:03
 */
@SpringBootApplication
public class AgentApp
{
    public static void main(String[] args) {
        SpringApplication.run(AgentApp.class, args);
    }
}
